<?php
/**
 * Created by PhpStorm.
 * User: Didier_TA
 * Date: 27/03/2015
 * Time: 08:45
 */

require_once 'save_functions.php';
require_once 'global_var.php';


class ActiveRecordCSV {
    public function save() {
        $tmp = $this;

        $data = get_object_vars($tmp);

        $informationsOfObject = array();

        $newId = getLastId($tmp::$file) + 1;
        $data["id"] = $newId;

        foreach ($data as $key => $value) {
            array_push($informationsOfObject, $value);

        }

        global $delimiter;

        $lineToWrite = implode($delimiter, $informationsOfObject);

        $put = file_put_contents($tmp::$file, $lineToWrite . "\n", FILE_APPEND);

    } // save()


    public function update() {
        $tmp = new static();
        $data = get_object_vars($tmp);

        $idOfObjectToUpdate = $data[0];


    }



    static function find_all() {
        $tmp = new static();
        // $objectInfos = array("id","prenom", "nom", "sexe", "photo");

        $objects = array();

        $object = array();

        // echo $tmp::$file . "<br>";

        $f = fopen($tmp::$file, 'r');
        do {
            $readLine = fgets($f);
            if(trim($readLine) == "") continue;
            $arrayOfAllContents = explode(" | ", $readLine);
            $nbColumns = count($arrayOfAllContents);

            for ($i = 0 ; $i < $nbColumns ; $i++) {
                $object[$i] = trim($arrayOfAllContents[$i], " ");
            }
            array_push($objects, $object);


        } while ( ! feof($f));

        $nbObjects = count($objects);


        for($i = 0 ; $i < $nbObjects ; $i++) {
            for ($j = 0 ; $j < $nbColumns ; $j++) {
                echo $objects[$i][$j];
                if($j != $nbColumns - 1) {
                    echo " || ";
                }
            }
            echo "<br><br>";
        }

        fclose($f);

        // return $objects;

    } // find_all()


} // C_ActiveRecordCSV()