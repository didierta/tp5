<?php
	require_once 'common_functions.php';

	function ajouterPortrait () {
		if (! ( isset($_POST["nom"]) &&
				isset($_POST["prenom"]) &&
				isset($_POST["sexe"]) &&
				isset($_FILES["photo"])
			  )
		   ) 
		{
			echo "Veuillez bien remplir tous les champs";
			return;
			
		}
		
		global $fileName;
		if(! file_exists($fileName)) {
			$errorMessage = "File does not exist";
			echo $errorMessage;
			return FALSE;
			
		}
		
		$nom = strtoupper(trim($_POST["nom"]));
		$prenom = strtoupper(trim($_POST["prenom"]));
		$sexe = strtoupper(trim($_POST["sexe"]));

		if(existsInFile($fileName, $nom, $prenom, $sexe)) {
			echo "Already saved in file!<br>";
			return FALSE;
		}

		$id = createId($fileName);

		// add in file
		if(isset($_FILES)) {
			$added = save_profile_in_file($id, $prenom, $nom, $sexe);
			return $added != FALSE;

		} // if
		 
	} // ajouterPortrait()

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
	
	function save_profile_in_file($id, $prenom, $nom, $sexe) {
		$directoryOfJPG = "./img";

		$result = savePictureIn($directoryOfJPG);

		if($result == FALSE) {
			echo "erreur du téléchargement du fichier<br>";
			return FALSE;		
		}
		
		global $delimiter;
		global $fileName;

		$newName = $_FILES["photo"]["name"];
		$newName = "../tp4/exo1/img/" . $newName;

		$personne = $id . $delimiter . 
					$prenom . $delimiter . 
					$nom . $delimiter . 
					$sexe . $delimiter . 
					$newName;

		$file = fopen($fileName, "a");
		$line = fgets($file);

		fwrite($file, $personne . "\n");

		fclose($file);


		return $added;
	}
	
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
	function existsInFile($fileName, $nom, $prenom, $sexe) {	 
		$file = fopen($fileName, 'r+');
		 
		$arrayOfAllContents = array();
		 
		$personne = array();
		 
		global $personneInfos;
		 
		$personnes = array();
		 
		do {
			$line = fgets($file);
			$arrayOfAllContents = explode("|", $line);
			
			$nbColumns = count($arrayOfAllContents);
		
		
			for ($i = 0 ; $i < $nbColumns ; $i++) {
				$personne[$personneInfos[$i]] = trim($arrayOfAllContents[$i]);
			}
		
			array_push($personnes, $personne);
		
		} while (!feof($file));
		
		fclose($file);
		foreach ($personnes as $personne) {
			if($personne["prenom"] == $prenom &&
			   $personne["nom"]    == $nom    &&
			   $personne["sexe"]   == $sexe
			   ) 
			{
			   	return true;
			}
		}
		
		return false;
		
	} // existsInFile()

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function getLastIdFromGalerie($fileName) { // works
    	$file = fopen($fileName, 'r+');
    	
    	$arrayOfAllContents = array();
    	
    	$personne = array();
    	
    	// $personneInfos = array("id","prenom", "nom", "sexe", "photo");
    	global $personneInfos;
    	
    	$nbColumns = count($personneInfos);
    	
    	
    	do {
    		$line = fgets($file);
    		if(trim($line) == "") continue;
    		$arrayOfAllContents = explode("|", $line);
    		
    		for ($i = 0 ; $i < $nbColumns ; $i++) {
    			if($personneInfos[$i] === "id") {
    				$arrayOfAllContents[$i] = intval($arrayOfAllContents[$i]);
    			}
    			$personne[$personneInfos[$i]] = $arrayOfAllContents[$i];
    		}
    		
    	} while (!feof($file));

    	$lastId = $personne["id"];
    	fclose($file);

    	return $lastId;	
    }

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function createId($fileName) {
    	$id = 0;
   		if (isEmptyFile($fileName)) {
			$id = 1;

		} else {
			$id = getLastIdFromGalerie($fileName) + 1;
		}

		return $id;
    }

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------


?>