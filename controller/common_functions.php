<?php
	function writeAllPersonsIn($fileName, $arrayOfPersonne) {
		global $delimiter;

		// $file = fopen($fileName, "w+");
		foreach ($arrayOfPersonne as $personne) {
			$id = $personne["id"];
			$lastName = $personne["nom"];
			$firstName = $personne["prenom"];
			$gender = $personne["sexe"];
			$jpgPath = $personne["photo"];

			$line = $id . $delimiter .
					$firstName . $delimiter .
					$lastName . $delimiter .
					$gender . $delimiter .
					$jpgPath;

			$written = file_put_contents($fileName, $line, FILE_APPEND);
		}
	} // writeAllPersonsIn()

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	function isNotEmpty($input) {
		$strTemp = $input;
		$strTemp = trim($strTemp);
	
		return !empty($strTemp);
	}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	function emptyFile($fileName) {
		$file = fopen($fileName, "w");
		fclose($file);
	}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

	function isEmptyFile($fileName) {
		return filesize($fileName) == 0;
	}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
	
	function currentLineIsEmpty($line) {
		$sizeOfLine = strlen(trim($line));
		return $sizeOfLine == 0;
	}


//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function savePictureIn($directoryOfJPG) {
		$directoryOfJPG = "./img";

		$tmpName = $_FILES["photo"]["tmp_name"];
		$newName = $_FILES["photo"]["name"];
		$result = move_uploaded_file($tmpName, "$directoryOfJPG/$newName");

		return $result; 	
    }


?>