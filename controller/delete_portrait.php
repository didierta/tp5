<?php
    require_once 'common_functions.php';

	function delete($idToDelete) {
		global $fileName;
		$idOfPersonneToDelete = trim($idToDelete);
		$personnes = createTableOfPersonne($fileName, $idOfPersonneToDelete);
		emptyFile($fileName);
		writeAllPersonsIn($fileName, $personnes);
        
	}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function createTableOfPersonne($fileName, $idToDelete) {
    	if(! file_exists($fileName)) {
    		$errorMessage = "File does not exist";
    		echo $errorMessage;
    		return;
    	}
        
    	global $personneInfos;

        $arrayOfAllContents = array();
        $personne = array();
    	$personnes = array();
        
        $file = fopen($fileName, 'r+');
    	
    	do {
    		$line = fgets($file);
            if(trim($line) == "") continue;
    		$arrayOfAllContents = explode("|", $line);
    		$nbColumns = count($arrayOfAllContents);

    		// do not take into account the person to delete
    		if(trim($arrayOfAllContents[0]) == $idToDelete) continue;
    		
    		for ($i = 0 ; $i < $nbColumns ; $i++) {
    			$personne[$personneInfos[$i]] = trim($arrayOfAllContents[$i], " ");
    		}
    		
    		array_push($personnes, $personne);
    		
    	} while (!feof($file));
    	
    	
    	fclose($file);

    	return $personnes;
    	
    } // createTableOfPersonne()


?>