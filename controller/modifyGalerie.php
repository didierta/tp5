<?php
    require_once 'common_functions.php';

	function modify($id) {
		global $fileName;
		$idOfPersonToModify = trim($id);
		$personnes = createTableOfPersonne_modify($fileName, $idOfPersonToModify);
		emptyFile($fileName);
		writeAllPersonsIn($fileName, $personnes);
        
	}

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function createTableOfPersonne_modify($fileName, $id) {
    	if(! file_exists($fileName)) {
    		$errorMessage = "File does not exist";
    		echo $errorMessage;
    		return;
    	}
    	global $personneInfos;
    	
    	$arrayOfAllContents = array();
    	$personne = array();
    	$personnes = array();
    	
    	$file = fopen($fileName, 'r+');
    	do {
    		$line = fgets($file);
            if(trim($line) == "") continue;
    		$arrayOfAllContents = explode("|", $line);
    		$nbColumns = count($arrayOfAllContents);

    		// take into account the person to modify
    		if(trim($arrayOfAllContents[0]) == $id) {
    			$personne["id"] = $id;
    			$personne["prenom"] = strtoupper(trim($_POST["prenom"]));
    			$personne["nom"] = strtoupper(trim($_POST["nom"]));
    			$personne["sexe"] = strtoupper(trim($_POST["sexe"]));

    			$saved = saveNewJPGFile();
    			if(! $saved) {
    				echo "Échec de la modification <br>";
    				fclose($file);
    				return;
    			}

				$newName = $_FILES["photo"]["name"];
				$newName = "../tp4/exo1/img/".$newName;
				$personne["photo"] = trim($newName)."\n";



    		} else {
    			for ($i = 0 ; $i < $nbColumns ; $i++) {
    				$personne[$personneInfos[$i]] = trim($arrayOfAllContents[$i], " ");
    			}
    		}

    		array_push($personnes, $personne);
    		
    	} while (!feof($file));
    	
    	fclose($file);

    	return $personnes;
    	
    } // createTableOfPersonne_modify()

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------

    function saveNewJPGFile() {
		if(isset($_FILES)) {
			$directoryOfJPG = "./img";

			$result = savePictureIn($directoryOfJPG);

			if($result == FALSE) {
				echo "erreur du téléchargement du fichier<br>";
				return FALSE;		
			}

			return true;

		} // if

    }

//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------


?>