<?php
	require_once 'global_var.php';

	function getLastId($fileName) {
		$f = fopen($fileName, 'r+');
    	
    	$arrayOfAllContents = array(); // contents of a single line in file

    	$object = array();

    	global $delimiter;

    	do {
    		$readLine = fgets($f);
    		if(trim($readLine) == "") continue;
    		$arrayOfAllContents = explode($delimiter, $readLine);
    		// var_dump($arrayOfAllContents);
    		// echo "<br><br>";

    		$nbColumns = count($arrayOfAllContents);

    		for ($i = 0 ; $i < $nbColumns ; $i++) {
    			if($i === 0 ) { // if id
    				$arrayOfAllContents[$i] = intval($arrayOfAllContents[$i]);
    			}
    			$object[$i] = $arrayOfAllContents[$i];
    		}


    	} while ( ! feof($f));

		$lastId = $object[0];
		// echo "Lasy ID : " . $lastId . "<br>"; // display last id

		return $lastId;

    	fclose($f);


	}


?>