<?php
/**
 * Created by PhpStorm.
 * User: Didier_TA
 * Date: 27/03/2015
 * Time: 09:10
 */


class Portrait extends ActiveRecordCSV {
    static $file = "../newGal.csv";

    var $id;
    var $lastName;
    var $firstName;
    var $gender;
    var $picture;
/*
    function __construct($id, $lastName, $firstName, $gender, $picture)
    {
        $this->id = $id;
        $this->lastName = $lastName;
        $this->firstName = $firstName;
        $this->gender = $gender;
        $this->picture = $picture;
    }
*/

    static function create($firstName, $lastName, $gender, $picture) {
    	$p = new Portrait();
    	// $p->id = $id;
    	$p->firstName = $firstName;
    	$p->lastName = $lastName;
    	$p->gender = $gender;
    	$p->picture = $picture;

    	return $p;
    }

}